import axios from 'axios';

export const endpoint = {
  scoring: '//scoringservice.moni.com.ar:7001/api/v1/scoring/',
  loans: 'https://wired-torus-98413.firebaseio.com/users'
};

export const rate = ({id, gender, email}) => axios.get(endpoint.scoring, {params: {'document_number': id, gender, email}}).then(r => r.data);
export const all = () => axios.get(`${endpoint.loans}.json`).then(r => r.data);
export const read = (id) => axios.get(`${endpoint.loans}/${id}.json`).then(r => ({...r.data, id}));
export const remove = (id) => axios.delete(`${endpoint.loans}/${id}.json`).then(r => r.data);
export const create = ({ name, surname, document_number, email}) => {
  return rate({document_number, email}).then(({approved}) => {
    const payload = {
      document_number,
      email,
      name,
      surname,
      status: approved ? 'Aprobado' : 'Rechazado'
    };
    return axios.post(`${endpoint.loans}.json`, payload).then(r => r.data)
  })
};
export const update = (id, data) => axios.put(`${endpoint.loans}/${id}.json`, data).then(r => r.data);

