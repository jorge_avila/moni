import { createSlice, createSelector } from 'redux-starter-kit'
import * as repo from './repositories';

const scores = createSlice({
  initialState: {},
  reducers: {
      init: (state, {payload}) => payload,
      add: (state, {payload}) => {
        const {id, ...data} = payload;
        state[payload.id] = data;
      },
      remove: (state, { payload }) => {
        delete state[payload]
      }
  },
  slice: 'scores'
})
  
const {actions, reducer} = scores;

const selectors = {
  ...scores.selectors,
  list: createSelector([scores.selectors.getScores], (items) => Object.keys(items).map(k => ({id:[k], ...items[k]}))),
  getById: (id, state) => scores.selectors.getScores(state)[id]
}

function fetchScores() {
  return (dispatch) => {
    repo.all()
      .then(res =>
        dispatch(actions.init(res))
      )
  }
}

function fetchScoresById(id) {
  return (dispatch) => {
    repo.read(id)
      .then(res =>
        dispatch(actions.add(res))
      )
  }
}

function createScore(data) {
  return (dispatch) => {
    return repo.create(data)
      .then(res =>
        {
          dispatch(actions.add(res))
        }
      )
  }
}

function updateScore(id, data) {
  return (dispatch) => {
    return repo.update(id, data)
      .then(res =>
        dispatch(actions.add(res))
      )
  }
}

function removeScore(id) {
  return (dispatch) => {
    return repo.remove(id)
      .then(res =>
        dispatch(actions.remove(id))
      )
  }
}

const effects = {
  fetchScores,
  fetchScoresById,
  createScore,
  updateScore,
  removeScore
}

export { actions, reducer, selectors, effects };
