import store from './store';
import {effects} from './scores';
import {create} from './repositories';

// this test will use store persistence
// repositories will be mocked as we dont want to test our third party providers
jest.mock('./repositories', () => ({
  create: jest.fn(),
}));

describe('core logic', () => {

  it('adds a score to the store when createScore is successful', (done) => {
    const response = {name: 'Jhon', surname: 'Lennon', id: 'test', status: 'Aprobado'};
    const expected = { test: {name: 'Jhon', surname: 'Lennon', status: 'Aprobado'}};

    create.mockReturnValueOnce(Promise.resolve(response))

    store.dispatch(effects.createScore(response))
    .then(() => {
      expect(store.getState().scores).toEqual(expected);
      done();
    });
  });
});