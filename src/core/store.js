import { configureStore } from 'redux-starter-kit'
import { reducer } from './scores';

const store = configureStore({
  reducer: { scores: reducer}
})

export default store;