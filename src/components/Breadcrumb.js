import React from 'react';
import { Route } from "react-router-dom";
import {BreadcrumbContainer, BreadcrumbItem} from 'components/Elements';

const Breadcrumb = () => {
    return <BreadcrumbContainer>
        <Route path="/solicitar-prestamo" render={() => <li><BreadcrumbItem to={'/'}>Home</BreadcrumbItem></li>} />
        <Route path="/solicitud" render={() => <li><BreadcrumbItem to={'/'}>Home</BreadcrumbItem></li>} />
        <Route path="/solicitud/:id" render={() => <li>-</li>} />
        <Route path="/solicitud/:id" render={({match}) => <li><BreadcrumbItem to={match.url}>Ver</BreadcrumbItem></li>} />
        <Route path="/solicitud/:id" render={({match}) => <li><BreadcrumbItem to={`${match.url}/edit`}>Editar</BreadcrumbItem></li>} />
    </BreadcrumbContainer>
};
export default Breadcrumb