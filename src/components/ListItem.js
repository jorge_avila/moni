import React from 'react';
import {ListItem as StyledItem, Icon} from 'components/Elements';

// age	"29"
// document_number	"33975127"
// email	"mariano.hell@gmail.com"
// gender	"M"
// name	"Mariano"
// status	"Aprobado"
// surname	"Argañaras"
const ListItem = ({email, id, name, surname, status}) => (
    <StyledItem to={`/solicitud/${id}`}>
        {`${name} ${surname}`}
        <strong>{email}</strong>
        <Icon fail={status !== 'Aprobado'}/>
    </StyledItem>
);

export default ListItem;