import React from 'react';
import { Form, Field, ErrorMessage } from 'formik';

import {Layout, StyledForm, ErrorMessage as StyledError} from 'components/Elements';

const MyForm = props => {
    const {
      isSubmitting,
      isUpdate
    } = props;
    return (
      <Layout>
        <StyledForm>
          <Form>
            <Field type="text" name="name" placeholder="Nombre"/>
            <ErrorMessage name="name" component={StyledError} />
            <Field type="text" name="surname" placeholder="Apellido"/>
            <ErrorMessage name="surname" component={StyledError} />
            <Field type="text" name="document_number" placeholder="DNI"/>
            <ErrorMessage name="document_number" component={StyledError} />
            <Field type="text" name="email" placeholder="Email"/>
            <ErrorMessage name="email" component={StyledError} />
            <button type="submit" disabled={isSubmitting}>{isUpdate ? 'Actualizar' : 'Aplicar'} solicitud</button>
          </Form>
        </StyledForm>
      </Layout>
    );
  };

  export default MyForm;