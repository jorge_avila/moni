import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Layout = styled.div`
  padding: 40px;
  max-width: 700px;
  margin: auto;
`;

export const CallToAction = styled(Link)`
  display: inline-block;
  font-family: sans-serif;
  padding: 1rem;
  text-decoration: none;
  color:#FFF;
  background: green;
  border: 1px solid green;
  border-radius: 5px;
`
export const ErrorMessage = styled.div`
  color:red;
  padding: 1rem 0;
  font-size: 0.7rem;
`;
export const Input = styled.input`
`
export const StyledForm = styled.div`
  form {
    display: flex;
    flex-flow: column;
  }
  input {
    margin-bottom: -1px;
    border: 1px solid #f3f3f3;
    background: #fafafa;
    padding: 1rem 1.2rem;
    &:focus {
      background: white;
    }
  }

  button {
    padding: 1rem;
    color: white;
    background: #0078ff;
    border: 1px solid #0101f3;
    border-radius: 5px;
    &:hover {
      cursor: pointer;
    }
  }
`

export const List = styled.div`
  font-family: sans-serif;
`;

export const ListItem = styled(Link)`
  padding: 1rem;
  margin-bottom: -1px;
  border: 1px solid #f3f3f3;
  background: white;
  display: grid;
  grid: auto-flow / 3fr 3fr 1fr;
`
export const Icon = styled.span`
  display: inline-block;
  border-radius: 50%;
  height: 1rem;
  width: 1rem;
  color: white;
  background: ${props => props.fail ? 'red' : 'black'};
  &::after {
    display: inline-block;
    content: "${props => props.fail ? '✕': '✓' }";
    color: white;
  }
`;

export const BreadcrumbItem = styled(Link)`
  font-family: sans-serif;
  text-decoration: none;
  color: #000;
  &:hover {
    color: blue;
  }
`;

export const BreadcrumbContainer = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
  margin-bottom: 2rem;
  li {
    margin-bottom: .5rem;
  }
`

export const RemoveBtn = styled.button`
  display: inline-block;
  color: black;
  background: transparent;
  border: 0;
  font-size: 1rem;
  padding: 0;
  cursor: pointer;

  &:hover {
    color: red;
  }
`

export const Hero = styled.div`
  padding: 2rem;
  font-size: 1.25rem;
  font-family: sans-serif;
  margin-top: 2rem;
  background: #e6e6e6;
`