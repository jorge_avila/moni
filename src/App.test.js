import React from 'react';
import ReactDOM from 'react-dom';
import {MemoryRouter} from 'react-router-dom';
import renderer from 'react-test-renderer';
import {Provider} from 'react-redux';
import moxios from 'moxios';

import App, {Routes} from './App';
import store from './core/store';

// highlevel testing of integration between our components

describe('App', () => {
  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  
  // snapshot testing of what's being rendered on each page
  // check __snapshots__ directory
  const routing = [
    {
      title: 'renders new score form',
      url: '/solicitar-prestamo',
      response: {}, 
    },
    {
      title: 'renders a positive view for a score',
      url: '/solicitud/test',
      response: {name:'Jhon', surname: 'Lennon', email:'jhon.lennon@stars.com', status: 'Aprobado'}, 
    },
    {
      title: 'renders a negative view for a score',
      url: '/solicitud/test',
      response: {name:'Jhon', surname: 'Lennon', email:'jhon.lennon@stars.com', status: 'Rechazado'}, 
    }
  ].forEach(({title, url, response}) => {
    it(title, (done) => {
      const component = renderer.create(
        <Provider store={store}>
          <MemoryRouter initialEntries={[url]}>
            <Routes />
          </MemoryRouter>
        </Provider>
      );
    
    moxios.wait(function () {
      let request = moxios.requests.mostRecent()
      if (!request) {
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
        return done();
      }
      request.respondWith({
        status: 200,
        response
      }).then(function () {
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
        done()
      })
    })
    })
  });
});