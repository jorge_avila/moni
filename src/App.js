import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Provider} from 'react-redux';

import store from './core/store';

import Home from './pages/Home';
import NewScore from './pages/Score/New';
import Score from './pages/Score';
import {Layout} from 'components/Elements';
import Breadcrumb from 'components/Breadcrumb';
export const Routes = () => (
  <Layout>
    <Breadcrumb />
    <Route path="/" exact component={Home}/>
    <Route path="/solicitar-prestamo" component={NewScore} />
    <Route path="/solicitud" component={Score} />
  </Layout>
);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Routes />
        </Router>
      </Provider>
    );
  }
}

export default App;
