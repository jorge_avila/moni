import React from 'react';
import ListScores from 'pages/ListScores';
import {CallToAction} from 'components/Elements';

const Home = () => (
  <div>
      <CallToAction to="/solicitar-prestamo">Solicitar prestamo</CallToAction>
      <ListScores />
  </div>
);

export default Home;