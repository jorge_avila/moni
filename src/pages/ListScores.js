import React from 'react';
import {effects, selectors} from 'core/scores';
import {connect} from 'react-redux';

import ListItem from 'components/ListItem';
import {List} from 'components/Elements';

class ListScores extends React.Component {
  componentDidMount() {
    this.props.getScores();  
  }

  render() {
    return (
      <List>
        <h3>Mis scores anteriores ({this.props.scores.length})</h3>
        {this.props.scores.map(({email, id, status, name, surname}) => <ListItem key={id} {...{name, surname, email, id, status}}/>)}
      </List>
    );
  }
}

export default connect((state) => ({
  scores: selectors.list(state),
}), { getScores: effects.fetchScores })(ListScores)