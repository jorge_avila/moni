import React from 'react';
import { withFormik } from 'formik';
import ScoreForm from 'components/Form';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {selectors, effects} from 'core/scores';
  

class Edit extends React.Component {
    render() {
        const {updateScore, history, match, score} = this.props;
        return <div>
           <Form {...{updateScore, history, match, score}} isUpdate/>
        </div>
    }
}

const formikDef = {
    enableReinitialize: true,
    mapPropsToValues: (props) => {
        return props.score || { name: '', surname: '', document_number: '', email: '' };
    },
    validate: values => {
      const errors = {};
      
      if (!values.name) {
        errors.name = 'Variante de validación';
      }
  
      return errors;
    },
    
    handleSubmit: (values, { setSubmitting, props }) => {
      props.updateScore(props.match.params.id, values)
      .then(r => {
        setSubmitting(false);
        props.history.push('/')
      })
    },
    
    displayName: 'BasicForm',
  };

const Form = withFormik(formikDef)(ScoreForm);

const withRedux = connect((state, props) =>
  ({
    score: selectors.getById(props.match.params.id, state),
  }),
  {
    updateScore: effects.updateScore,
    getScoreById: effects.fetchScoresById,
  });


export default compose(withRedux, withRouter)(Edit);
  