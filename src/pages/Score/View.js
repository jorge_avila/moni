import React from 'react';
import {connect} from 'react-redux';

import {effects, selectors} from 'core/scores';
import {Hero} from 'components/Elements';

const ScoreView = ({status, name}) => <Hero>{`Hey ${name} tu prestamo fue ${status}`}</Hero>

class ViewScore extends React.Component {
  render() {
    const { score } = this.props;
    if (!score) return null;
    const { status, name} = score;
    return <div>
      {status && name && <ScoreView {...{status, name}}/>}
      {!status && !name && 'No se encuentra el resultado'}
    </div>
  }
}

export default connect((state, props) =>
  ({
    score: selectors.getById(props.match.params.id, state),
  }),
  {
    getScoreById: effects.fetchScoresById,
  })(ViewScore)