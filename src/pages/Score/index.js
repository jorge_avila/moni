import React from 'react';
import { withRouter, Route } from "react-router-dom";
import {connect} from 'react-redux';
import {selectors, effects} from 'core/scores';
import View from './View';
import Edit from './Edit';
import {RemoveBtn} from 'components/Elements';


class GetScore extends React.Component {
  componentDidMount() {
    this.props.getScoreById(this.props.match.params.id);
  }
  remove = () => {
    this.props.removeScore(this.props.match.params.id)
      .then(r => {
        this.props.history.push('/')
      })
      .catch(r => console.error(r))
  }
  render() {
    if (this.props.score) return <RemoveBtn onClick={this.remove}>Eliminar</RemoveBtn>
    return null;
  }
}

const CommonScoreLogic = connect((state, props) => ({
  score: selectors.getById(props.match.params.id, state)
}), {
  getScoreById: effects.fetchScoresById,
  removeScore: effects.removeScore
})(withRouter(GetScore));

const Score = ({match}) =>(
  <div>
    <Route path={`${match.path}/:id`} component={CommonScoreLogic} />
    <Route path={`${match.path}/:id/edit`} component={Edit} />
    <Route
      exact
      path={`${match.path}/:id`}
      component={View}
    />
  </div>
)

export default Score;