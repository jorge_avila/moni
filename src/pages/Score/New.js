import { withFormik } from 'formik';
import ScoreForm from 'components/Form';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {effects} from 'core/scores';
  
const withForm = withFormik({
  mapPropsToValues: () => ({ name: '', surname: '', document_number: '', email: '' }),
  validate: values => {
    // mejor usar un validator schema
    const errors = {};
    
    if (!values.name) {
      errors.name = 'Requerido';
    }
    if (!values.document_number) {
      errors.document_number = 'Requerido';
    }
    if (!values.email) {
      errors.email = 'Requerido';
    }
    return errors;
  },
  
  handleSubmit: (values, { setSubmitting, props }) => {
    props.createScore(values)
    .then(r => {
      setSubmitting(false);
      props.history.push('/')
    })
  },
  
  displayName: 'BasicForm',
});

const withRedux = connect(null, {
  createScore: effects.createScore
})

  
export default compose(withRedux, withRouter)(withForm(ScoreForm));