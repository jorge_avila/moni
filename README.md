The setup of the project can be done with
### `npm install`

To run this app you can do:
### `npm start`

To run jest testing you can do:
### `npm run test`

### dont forget to disable CORS on your browser

## About
This project uses create-react-app boilerplate and redux-starter-kit to ease the setup time.

Directory structure:

```
  /src
    /componets (collection of visual styles and stateless components)
    /core (all our business logic should be made here)
    /pages (views with connection to core logic, also known as modules)
```

The main idea is to have our business logic readable and in one logic place, where more sensitive rules can be applied.
The third party apis were abstracted behind a local data repository.

This package uses redux, redux-thunk and a ducks file structure for global state management.
As for ethereal states such as forms we are using formik, which helps to abstract validation,
errors, and individual states of form elements.

One special case goes for the breadcrumb component, which I found that can be
maintained better when it's behaviour and business rules are defined in one place.


Testing:
Coverage is extremely low, `App.test.js` and `core.test.js` test files were created
to have, a high level, integration test coverage of our most important components

